import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Router } from "@angular/router";
import { SearchBar } from "tns-core-modules/ui/search-bar";
import { ObservableProperty } from "../observable-property-decorator";
import { Observable } from 'tns-core-modules/data/observable';

@Component({
    selector: "Home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.scss"]
})
export class HomeComponent extends Observable implements OnInit {

    public breedlist: any;
    private isLoading: boolean = false;

    TEMP_breedlist: any;
    @ObservableProperty() search_text: string;
    search_text_filter = "";

    public constructor(private http: HttpClient, private router: Router) {
        super();
    }

    public ngOnInit(): void {
        this.getBreedList();
    }

    public getBreedList(){
        this.isLoading = true;
        this.http.get("https://dog.ceo/api/breeds/list")
        .pipe(map(result => (<any>result)))
        .subscribe(result => {
            this.breedlist = result.message;
            this.TEMP_breedlist = result.message;
            this.isLoading = false;
        }, error => {
            // console.error(error);
            alert("Sorry for the inconvenience! contact us for more info...");
        });
    }

    onBreedTap(args) {
        this.router.navigate(["home/breedview", args]);
    }

    public search_filter(){
        let TEMP_breedlist = this.breedlist;
        if(this.search_text_filter){

            let temp_search_text_filter = this.search_text_filter;

            this.TEMP_breedlist = this.breedlist.filter(function (temp_list) {
                return temp_list.toLowerCase().includes(temp_search_text_filter);
            });
        }
        return this.TEMP_breedlist;
    }

    search_onSubmit(args) {
        const searchBar = args.object as SearchBar;
        this.search_text_filter = searchBar.text;
        this.set("TEMP_breedlist", this.search_filter());
        searchBar.android.clearFocus();
    }

    search_onTextChanged(args) {
        const searchBar = args.object as SearchBar;
        this.search_text_filter = searchBar.text;
        this.set("TEMP_breedlist", this.search_filter());
    }

    search_onClear(args) {
        const searchBar = args.object as SearchBar;
        searchBar.android.clearFocus();
        this.search_text_filter = "";
        this.set("TEMP_breedlist", this.breedlist);
    }
}